import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";

import NewCardSkeleton from "../util/NewCardSkeleton";

import { connect } from "react-redux";

import ProfileCard from "../components/profile/ProfileCard";

export class NewHome extends Component {
  render() {
    return (
      <Grid container spacing={2}>
        <Grid item md={3} sm={2}></Grid>
        <Grid item md={6} sm={8} xs={12}>
          <NewCardSkeleton />
        </Grid>
        <Grid item md={3} sm={2}></Grid>
      </Grid>
    );
  }
}

NewHome.propTypes = {
  data: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.data,
});

export default connect(mapStateToProps)(NewHome);
