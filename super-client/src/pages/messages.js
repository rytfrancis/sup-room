import React, { Component } from "react";

export class Messages extends Component {
  render() {
    return (
      <div>
        <div style={{ textAlign: "center", padding: 50 }}>
          Your messages will go here...
        </div>
      </div>
    );
  }
}

export default Messages;
