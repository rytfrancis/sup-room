import React from "react";
import "./App.css";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import themeFile from "./util/theme";
import jwtDecode from "jwt-decode";
import axios from "axios";
//Redux
import { Provider } from "react-redux";
import store from "./redux/store";
import { SET_AUTHENTICATED } from "./redux/types";
import { logoutUser, getUserData } from "./redux/actions/userActions";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
//components
import Navbar from "./components/layout/Navbar";
import AuthRoute from "./util/AuthRoute";
//pages
import Home from "./pages/home";
import Login from "./pages/login";
import Signup from "./pages/signup";
import User from "./pages/user";
import Profile from "./components/profile/Profile";
import Messages from "./pages/messages";
import Favorites from "./pages/favorites";
import NewHome from "./pages/newHome";
import Welcome from "./pages/welcome";

// axios.defaults.baseURL =
//   "https://europe-west1-super-roomie-dev.cloudfunctions.net/api";

const theme = createMuiTheme(themeFile);

const token = localStorage.getItem("FBIdToken");
if (token) {
  const decodedToken = jwtDecode(token);
  // console.log(decodedToken);
  if (decodedToken.exp * 1000 < Date.now()) {
    store.dispatch(logoutUser());
    window.location.href = "/login";
  }
  store.dispatch({ type: SET_AUTHENTICATED });
  axios.defaults.headers.common["Authorization"] = token;
  store.dispatch(getUserData());
}

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <Router>
          <Navbar />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Home} />
              <AuthRoute exact path="/login" component={Login} />
              <AuthRoute exact path="/signup" component={Signup} />
              {/* update route to use Profile component and update profile component */}
              <Route exact path="/profile" component={Profile} />
              <Route exact path="/users/:handle" component={User} />

              <Route exact path="/messages" component={Messages} />
              <Route exact path="/favorites" component={Favorites} />
              <Route exact path="/newhome" component={NewHome} />
              <Route
                exact
                path="/welcome"
                render={(props) => (
                  <Profile
                    newUser={false}
                    testQuestionsSelect={true}
                    testQuestionsRadio={false}
                  />
                )}
              />
            </Switch>
          </div>
        </Router>
      </Provider>
    </MuiThemeProvider>
  );
}

export default App;
