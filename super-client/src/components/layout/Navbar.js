import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import MyButton from "../../util/MyButton";

//MUI stuff
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

//icons
import HomeIcon from "@material-ui/icons/Home";
import FaceIcon from "@material-ui/icons/Face";
import StarsIcon from "@material-ui/icons/Stars";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import HomeWorkIcon from "@material-ui/icons/HomeWork";

export class Navbar extends Component {
  render() {
    const { authenticated } = this.props;
    return (
      <AppBar>
        <Toolbar className="nav-container">
          {authenticated ? (
            <Fragment>
              <Link to="/">
                <MyButton tip="Home">
                  <HomeIcon />
                </MyButton>
              </Link>

              <Link to="/favorites">
                <MyButton tip="Favorites">
                  <StarsIcon />
                </MyButton>
              </Link>
              <Link to="/messages">
                <MyButton tip="Messages">
                  <MailOutlineIcon />
                </MyButton>
              </Link>

              <Link to="/profile">
                <MyButton tip="Profile">
                  <FaceIcon />
                </MyButton>
              </Link>
              <Link to="/newhome">
                <MyButton tip="New Home">
                  <HomeWorkIcon />
                </MyButton>
              </Link>
            </Fragment>
          ) : (
            <Fragment>
              <Button color="inherit" component={Link} to="/login">
                Login
              </Button>
              <Button color="inherit" component={Link} to="/">
                Home
              </Button>
              <Button color="inherit" component={Link} to="/signup">
                Signup
              </Button>
            </Fragment>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

Navbar.propTypes = {
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps)(Navbar);
