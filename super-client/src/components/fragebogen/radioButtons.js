import React from "react";
// import { withStyles } from "@material-ui/core/styles";
// import { green } from "@material-ui/core/colors";
import Radio from "@material-ui/core/Radio";

export default function RadioButtons() {
  const [selectedValue, setSelectedValue] = React.useState("a");

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };

  return (
    <div>
      <Radio
        checked={selectedValue === "a"}
        onChange={handleChange}
        value="a"
        name="radio-button-demo"
        inputProps={{ "aria-label": "A" }}
      />
      <Radio
        checked={selectedValue === "b"}
        onChange={handleChange}
        value="b"
        name="radio-button-demo"
        inputProps={{ "aria-label": "B" }}
      />
      <Radio
        checked={selectedValue === "c"}
        onChange={handleChange}
        value="c"
        name="radio-button-demo"
        inputProps={{ "aria-label": "C" }}
      />
      <Radio
        checked={selectedValue === "d"}
        onChange={handleChange}
        value="d"
        // color="default"
        name="radio-button-demo"
        inputProps={{ "aria-label": "D" }}
      />
      <Radio
        checked={selectedValue === "e"}
        onChange={handleChange}
        value="e"
        // color="default"
        name="radio-button-demo"
        inputProps={{ "aria-label": "E" }}
        // size="small"
      />
      <Radio
        checked={selectedValue === "f"}
        onChange={handleChange}
        value="f"
        // color="default"
        name="radio-button-demo"
        inputProps={{ "aria-label": "F" }}
      />
      <Radio
        checked={selectedValue === "g"}
        onChange={handleChange}
        value="g"
        // color="default"
        name="radio-button-demo"
        inputProps={{ "aria-label": "G" }}
        // size="small"
      />
    </div>
  );
}
