import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
//redux
import { connect } from "react-redux";
import { editUserDetails } from "../../redux/actions/userActions";
//mui
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import MyButton from "../../util/MyButton";
//icons
import EditIcon from "@material-ui/icons/Edit";

import RadioButtons from "../fragebogen/radioButtons";
// import classes from "*.module.css";

const styles = (theme) => ({
  ...theme.spreadThis,
  button: {
    float: "right",
  },
  formControl: {
    // border: "1px solid black",
    display: "block",
    margin: "0 auto",
    margin: theme.spacing(1),
    marginBottom: 25,
    minWidth: "70%",
  },
  formLabel: {
    marginBottom: 10,
  },
  radioButtons: {
    margin: "0 auto",
    // marginLeft: 25,
    // border: "1px solid black",
    width: 300,
  },
  //   selectEmpty: {
  //     marginTop: theme.spacing(2),
  //   },
});

class EditDetails extends Component {
  state = {
    dob: "",
    gender: "",
    location: "",
    offering: null,
    headline: "",
    open: this.props.testQuestionsOpen,
    first: "",
    second: "",
    third: "",
    fourth: "",
    // value: "",
    selectedValue: "",
  };

  mapUserDetailsToState = (credentials) => {
    this.setState({
      dob: credentials.dob ? credentials.dob : "",
      gender: credentials.gender ? credentials.gender : "",
      location: credentials.location ? credentials.location : "",
      offering: credentials.offering ? credentials.offering : "",
      headline: credentials.headline ? credentials.headline : "",
    });
  };
  handleOpen = () => {
    this.setState({
      open: true,
    });
    this.mapUserDetailsToState(this.props.credentials);
  };
  handleClose = () => {
    //whatever
    this.setState({ open: false });
  };
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSubmit = () => {
    const userDetails = {
      dob: this.state.dob,
      gender: this.state.gender,
      location: this.state.location,
      offering: this.state.offering,
      headline: this.state.headline,
    };
    this.props.editUserDetails(userDetails);
    this.handleClose();
  };

  componentDidMount() {
    const { credentials } = this.props;
    this.mapUserDetailsToState(credentials);
  }

  render() {
    const { classes } = this.props;
    const { value, selectedValue } = this.state;
    const { handleChange } = this;

    // const [selectedValue, setSelectedValue] = React.useState("a");

    // const handleChange = (event) => {
    //   setSelectedValue(event.target.value);
    // };
    return (
      <Fragment>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>Questionnaire - page 4 of 6</DialogTitle>
          <DialogContent>
            <form>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>
                  I prefer a nice choice of words.
                </FormLabel>
                <div className={classes.radioButtons}>
                  <RadioButtons />
                </div>
              </FormControl>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>
                  I keep the kitchen clean.
                </FormLabel>
                <div className={classes.radioButtons}>
                  <RadioButtons />
                </div>
              </FormControl>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>
                  I pay rent on time.
                </FormLabel>
                <div className={classes.radioButtons}>
                  <RadioButtons />
                </div>
              </FormControl>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>
                  I show consideration for the roommates.
                </FormLabel>
                <div className={classes.radioButtons}>
                  <RadioButtons />
                </div>
              </FormControl>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>
                  I am especially considerate of other people's items.
                </FormLabel>
                <div className={classes.radioButtons}>
                  <RadioButtons />
                </div>
              </FormControl>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>
                  I always clean up after myself.
                </FormLabel>
                <div className={classes.radioButtons}>
                  <RadioButtons />
                </div>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Save & Close
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Next
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

EditDetails.propTypes = {
  editUserDetails: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  credentials: state.user.credentials,
});

export default connect(mapStateToProps, { editUserDetails })(
  withStyles(styles)(EditDetails)
);
