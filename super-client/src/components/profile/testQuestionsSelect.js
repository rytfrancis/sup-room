import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
//redux
import { connect } from "react-redux";
import { editUserDetails } from "../../redux/actions/userActions";
//mui
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import MyButton from "../../util/MyButton";
//icons
import EditIcon from "@material-ui/icons/Edit";
// import classes from "*.module.css";

const styles = (theme) => ({
  ...theme.spreadThis,
  button: {
    float: "right",
  },
  formControl: {
    // border: "1px solid black",
    // margin: "0 auto",
    margin: theme.spacing(1),
    marginBottom: 60,
    minWidth: "98%",
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
});

class EditDetails extends Component {
  state = {
    dob: "",
    gender: "",
    location: "",
    offering: null,
    headline: "",
    open: this.props.testQuestionsOpen,
    first: "",
    second: "",
    third: "",
    fourth: "",
  };

  mapUserDetailsToState = (credentials) => {
    this.setState({
      dob: credentials.dob ? credentials.dob : "",
      gender: credentials.gender ? credentials.gender : "",
      location: credentials.location ? credentials.location : "",
      offering: credentials.offering ? credentials.offering : "",
      headline: credentials.headline ? credentials.headline : "",
    });
  };
  handleOpen = () => {
    this.setState({
      open: true,
    });
    this.mapUserDetailsToState(this.props.credentials);
  };
  handleClose = () => {
    //whatever
    this.setState({ open: false });
  };
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSubmit = () => {
    const userDetails = {
      dob: this.state.dob,
      gender: this.state.gender,
      location: this.state.location,
      offering: this.state.offering,
      headline: this.state.headline,
    };
    // const userDetails = {
    //   dob: "06 02 1986",
    //   gender: "male",
    //   location: "Berlin, Germany",
    //   offering: true,
    // };
    //update to add details to state so new call doesn't need to be made to retrieve. redirect to /profile
    this.props.editUserDetails(userDetails);
    this.handleClose();
  };

  componentDidMount() {
    const { credentials } = this.props;
    this.mapUserDetailsToState(credentials);
  }

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        {/* <MyButton
          tip="Edit Details"
          onClick={this.handleOpen}
          btnClassName={classes.button}
        >
          <EditIcon color="primary" />
        </MyButton> */}
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>Living Preferences - page 2 of 2</DialogTitle>
          <DialogContent>
            <form>
              {/* <FormControl>
                <InputLabel id="first-question-label">Cleanliness</InputLabel>
                <Select
                  labelId="first-question-label"
                  id="first-question"
                  name="dob"
                  // type="text"
                  label="Birthdate"
                  // multiline
                  // placeholder="DD MM YYYY"
                  // className={classes.textField}

                  value={"Ryan"}
                  onChange={this.handleChange}
                  fullWidth
                >
                  <MenuItem value={1}>Most clean</MenuItem>
                  <MenuItem value={2}>Vary Dirty</MenuItem>
                </Select>
              </FormControl>
              <br /> */}
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">
                  Ideal number of roomies
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  name="first"
                  value={this.state.first}
                  onChange={this.handleChange}
                >
                  <MenuItem value={10}>1</MenuItem>
                  <MenuItem value={20}>1-2</MenuItem>
                  <MenuItem value={30}>2-3</MenuItem>
                </Select>
              </FormControl>

              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">
                  Prefered neighborhoods
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  name="second"
                  value={this.state.second}
                  onChange={this.handleChange}
                >
                  <MenuItem value={10}>I don't have a preference.</MenuItem>
                  <MenuItem value={20}>Kreuzberg</MenuItem>
                  <MenuItem value={30}>Friedrichshain</MenuItem>
                  <MenuItem value={40}>
                    As long as it's in the ring, I don't care.
                  </MenuItem>
                </Select>
              </FormControl>

              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">
                  Do you have any pets?
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={this.state.third}
                  name="third"
                  onChange={this.handleChange}
                >
                  <MenuItem value={10}>No</MenuItem>
                  <MenuItem value={20}>Yes</MenuItem>
                  <MenuItem value={30}>No, but I'd like to get one.</MenuItem>
                </Select>
              </FormControl>

              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">
                  Do you mind living with pets?
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={this.state.fourth}
                  name="fourth"
                  onChange={this.handleChange}
                >
                  <MenuItem value={10}>I have pets.</MenuItem>
                  <MenuItem value={20}>I love living with animals.</MenuItem>
                  <MenuItem value={30}>I can't. I'm allergic.</MenuItem>
                  <MenuItem value={40}>
                    I'm fine as long as I don't have to take care of them.
                  </MenuItem>
                </Select>
              </FormControl>

              {/* <TextField
                name="offering"
                type="text"
                label="Offering room or looking?"
                placeholder="Offering"
                className={classes.textField}
                value={this.state.offering}
                onChange={this.handleChange}
                fullWidth
              />
              <TextField
                name="headline"
                type="text"
                label="Headline"
                multiline
                rows="3"
                placeholder="Feel free to add a headline to your profile"
                className={classes.textField}
                value={this.state.headline}
                onChange={this.handleChange}
                fullWidth
              /> */}
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Save & Close
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Next
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

EditDetails.propTypes = {
  editUserDetails: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  credentials: state.user.credentials,
});

export default connect(mapStateToProps, { editUserDetails })(
  withStyles(styles)(EditDetails)
);
