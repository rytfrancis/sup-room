const functions = require("firebase-functions");
// const admin = require('firebase-admin');
const app = require("express")();

const { db } = require("./util/admin");

const FBAuth = require("./util/fbAuth");

const {
    signup,
    login,
    uploadImage,
    addUserDetails,
    getAuthenticatedUser,
    getUserDetails,
    getAllUsersTest,
} = require("./handlers/users");

//users routes
app.post("/signup", signup);
app.post("/login", login);
app.post("/user/image", FBAuth, uploadImage);
app.post("/user", FBAuth, addUserDetails);
app.get("/user", FBAuth, getAuthenticatedUser);
app.get("/user/:handle", getUserDetails);
app.get("/allusers", getAllUsersTest);

exports.api = functions.region("europe-west1").https.onRequest(app);

exports.onUserImageChange = functions
    .region("europe-west1")
    .firestore.document("/users/{userId}")
    .onUpdate((change) => {
        console.log(change.before.data());
        console.log(change.after.data());
        if (change.before.data().imageUrl !== change.after.data().imageUrl) {
            console.log("Image has changed");
            let batch = db.batch();
            return db
                .collection("details")
                .where("userHandle", "==", change.before.data().handle)
                .get()
                .then((data) => {
                    data.forEach((doc) => {
                        const detail = db.doc(`/details/${doc.id}`);
                        batch.update(detail, {
                            userImage: change.after.data().imageUrl,
                        });
                    });
                    return batch.commit();
                });
        } else {
            return true;
        }
    });
